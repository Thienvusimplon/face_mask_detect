from feature_extraction_rgb import feature_extract
from classification import classify
import pandas as pd

dataset = pd.read_csv("../dataset/train.csv")
dataset = dataset[dataset["classname"].isin(["face_with_mask",
                                             "face_no_mask"])]

data = feature_extract(dataset)
classify(data)
