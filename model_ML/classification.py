from sklearn.svm import SVC
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.dummy import DummyClassifier
from sklearn.metrics import classification_report
from joblib import dump, load
import seaborn as sns
import matplotlib.pyplot as plt


def classify(data):

    X = data.drop(["pic_ref", "classname"], axis=1)
    y = data["classname"]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

    print('\n\nRépartition des classes :\n')
    print(y.value_counts().sort_values(ascending=True))

    dummy_clf = DummyClassifier(strategy="most_frequent")
    dummy_clf.fit(X, y)
    print(f'\nDummy score : {dummy_clf.score(X, y)}\n')

    svc_clf = make_pipeline(StandardScaler(), SVC(gamma="auto", C=1000))
    svc_clf.fit(X_train, y_train)
    y_pred_svc = svc_clf.predict(X_test)

    print(f'SVC classifier metrics :\n')
    print(classification_report(y_test, y_pred_svc, target_names=y.unique()))

    dump(svc_clf, 'svc_clf.joblib')
    return
